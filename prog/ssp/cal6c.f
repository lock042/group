C
C *** CALCUL DES 6C , STOCKAGE DES CODES ET DES VALEURS
C
      SUBROUTINE CAL6C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      INTEGER I6C                                                       INTEGER*4
      COMMON /COM6C/ V6C(NB6C),I6C(NB6C)
      INTEGER CTR
      DOUBLE PRECISION SXC1
C
      N=0
      DO 1 IC1=1,MXSYM
        DO 2 IC2=1,MXSYM
          DO 3 IC3=1,MXSYM
            IF(CTR(IC1,IC2,IC3).EQ.0) GOTO 3
            DO 4 IC4=1,MXSYM
              DO 5 IC5=1,MXSYM
                IF(CTR(IC4,IC5,IC3).EQ.0) GOTO 5
                DO 6 IC6=1,MXSYM
                  IF(CTR(IC4,IC2,IC6)*CTR(IC1,IC5,IC6).EQ.0) GOTO 6
                  N=N+1
                  I6C(N)=   100000*IC1 + 10000*IC2 + 1000*IC3 +
     &                      100*IC4    + 10*IC5    + IC6
                  V6C(N)=SXC1(IC1,IC2,IC3,IC4,IC5,IC6)
6               CONTINUE
5             CONTINUE
4           CONTINUE
3         CONTINUE
2       CONTINUE
1     CONTINUE
C     PRINT *, 'CAL6C  => NB6C=',N
      RETURN
      END
