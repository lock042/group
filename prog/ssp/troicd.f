C
C     *****************************************************************
C
C     SOUS-PROGRAMME DE RECHERCHE DES 3C PAR DICHOTOMIE.
C
C     (SI LE 3C N'EST PAS DANS LE TABLEAU, IL EST RESTITUE NUL)
C
C     *****************************************************************
C
      SUBROUTINE TROICD(IC1,IC2,IC3,IB,IS1,IS2,IS3,TC,IPTC)
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      COMMON /COMT/ T(MXTRCD),IT(MXTRCD),ICT(MXTRCD)
C
C     ----- INITIALISATIONS
C
      TC=0.D0
      IPTC=0
C
C     ----- CODE
C
      ICODE=IC1*1000000+IC2*100000+IC3*10000+IB*1000+IS1*100+IS2*10+IS3
C
C     ----- RECHERCHE
C
      I1=1
      I2=MXTRCD
      IF(ICODE.LT.ICT(I1)) GOTO 10
      IF(ICODE.GT.ICT(I2)) GOTO 10
      IF(ICODE.EQ.ICT(I2)) GOTO 4
1     I=(I1+I2)/2
      IF(ICODE-ICT(I)) 2,5,3
2     IF((I2-I1).EQ.1) GOTO 10
      I2=I
      GOTO 1
3     IF((I2-I1).EQ.1) GOTO 10
      I1=I
      GOTO 1
4     I=I2
5     TC=T(I)
      IPTC=IT(I)
C
C     ----- FIN
C
10    RETURN
      END
