C
C *** TRANSFORMATION U-G
C
C SMIL CHAMPION DEC 78
C
      SUBROUTINE UG(ICC,ISS,IC,IS,S)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      IF(ICC-3)1,5,1
1     IF((ICC-2)*(ICC-5))3,2,3
2     IC=ICC-1
      GO TO 4
3     IC=ICC+1
4     IS=ISS
      S=1.D0
      RETURN
5     IC=ICC
      IF(ISS-1)6,6,7
6     IS=2
      S=-1.D0
      RETURN
7     IS=1
      S=1.D0
      RETURN
      END
