C
C *** MULTIPLICATION DANS Os (REPRESENTATIONS ENTIERES ET DEMI-ENTIERES).
C
C  REY M. ----> NOVEMBRE 99
C
C    A1  ------> 1
C    A2  ------> 2
C    E   ------> 3
C    F1  ------> 4
C    F2  ------> 5
C    E'1 ------> 6
C    E'2 ------> 7
C    G'  ------> 8
C
      SUBROUTINE MULOS(IC1,IC2,N,IC,NM,MDMIC)
      IMPLICIT INTEGER (I,N)
      INTEGER IC(MDMIC)
      INTEGER NM(MDMIC)
C
      IF(IC1-IC2)1,1,2
1     I1=IC1
      I2=IC2
      GO TO 3
2     I1=IC2
      I2=IC1
3     IF(I1-2)4,5,16
4     N=1
      IC(1)=I2
      NM(1)=1
      RETURN
5     N=1
      IF(I2-3)6,7,8
6     IC(1)=1
      NM(1)=1
      RETURN
7     IC(1)=3
      NM(1)=1
      RETURN
8     IF(I2-4)9,9,10
9     IC(1)=5
      NM(1)=1
      RETURN
10    IF(I2-6)11,12,13
11    IC(1)=4
      NM(1)=1
      RETURN
12    IC(1)=7
      NM(1)=1
      RETURN
13    IF(I2-7)14,14,15
14    IC(1)=6
      NM(1)=1
      RETURN
15    IC(1)=8
      NM(1)=1
      RETURN
16    IF(I1-4)17,25,32
17    IF(I2-3)18,18,19
18    N=3
      IC(1)=1
      IC(2)=2
      IC(3)=3
      NM(1)=1
      NM(2)=1
      NM(3)=1
      RETURN
19    IF(I2-6)20,21,22
20    N=2
      IC(1)=4
      IC(2)=5
      NM(1)=1
      NM(2)=1
      RETURN
21    N=1
      IC(1)=8
      NM(1)=1
      RETURN
22    IF(I2-7)23,23,24
23    N=1
      IC(1)=8
      NM(1)=1
      RETURN
24    N=3
      IC(1)=6
      IC(2)=7
      IC(3)=8
      NM(1)=1
      NM(2)=1
      NM(3)=1
      RETURN
25    IF(I2-5)26,27,28
26    N=4
      IC(1)=1
      IC(2)=3
      IC(3)=4
      IC(4)=5
      NM(1)=1
      NM(2)=1
      NM(3)=1
      NM(4)=1
      RETURN
27    N=4
      IC(1)=2
      IC(2)=3
      IC(3)=4
      IC(4)=5
      NM(1)=1
      NM(2)=1
      NM(3)=1
      NM(4)=1
      RETURN
28    IF(I2-7)29,30,31
29    N=2
      IC(1)=6
      IC(2)=8
      NM(1)=1
      NM(2)=1
      RETURN
30    N=2
      IC(1)=7
      IC(2)=8
      NM(1)=1
      NM(2)=1
      RETURN
31    N=4
      IC(1)=6
      IC(2)=7
      IC(3)=8
      IC(4)=8
      NM(1)=1
      NM(2)=1
      NM(3)=4
      NM(4)=5
      RETURN
32    IF(I1-6)33,39,43
33    IF(I2-6)34,35,36
34    N=4
      IC(1)=1
      IC(2)=3
      IC(3)=4
      IC(4)=5
      NM(1)=1
      NM(2)=1
      NM(3)=1
      NM(4)=1
      RETURN
35    N=2
      IC(1)=7
      IC(2)=8
      NM(1)=1
      NM(2)=1
      RETURN
36    IF(I2-7)37,37,38
37    N=2
      IC(1)=6
      IC(2)=8
      NM(1)=1
      NM(2)=1
      RETURN
38    N=4
      IC(1)=6
      IC(2)=7
      IC(3)=8
      IC(4)=8
      NM(1)=1
      NM(2)=1
      NM(3)=2
      NM(4)=3
      RETURN
39    IF(I2-7)40,41,42
40    N=2
      IC(1)=1
      IC(2)=4
      NM(1)=1
      NM(2)=1
      RETURN
41    N=2
      IC(1)=2
      IC(2)=5
      NM(1)=1
      NM(2)=1
      RETURN
42    N=3
      IC(1)=3
      IC(2)=4
      IC(3)=5
      NM(1)=1
      NM(2)=1
      NM(3)=1
      RETURN
43    IF(I1-7)44,44,47
44    IF(I2-7)45,45,46
45    N=2
      IC(1)=1
      IC(2)=4
      NM(1)=1
      NM(2)=1
      RETURN
46    N=3
      IC(1)=3
      IC(2)=4
      IC(3)=5
      NM(1)=1
      NM(2)=1
      NM(3)=1
      RETURN
47    N=7
      IC(1)=1
      IC(2)=2
      IC(3)=3
      IC(4)=4
      IC(5)=4
      IC(6)=5
      IC(7)=5
      NM(1)=1
      NM(2)=1
      NM(3)=1
      NM(4)=1
      NM(5)=1
      NM(6)=1
      NM(7)=1
      RETURN
      END
