C
C     ELEM. MAT. REDUIT DE L'OPERATEUR ELECTRONIQUE
C           (et rotationnel pour J demi entier.)
C
      FUNCTION EMRROD(K,B)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      COMMON /FA/ FACT(MXFAC),KFAC(MXFAC)
C
      I2P2=2*B+2
      I2PK1=2*B+K+1
      I2M1=2*K-1
      I2MK=2*B-K
      C=1.D0
      B=1.D0
      IF(I2MK-1)2,2,1
1     A=FACT(I2PK1)/FACT(I2MK)
      IA=KFAC(I2PK1)-KFAC(I2MK)
      GO TO 3
2     A=FACT(I2PK1)
      IA=KFAC(I2PK1)
3     A=A*(10.D0**IA)
      IF(K-1)6,6,4
4     DO 5 I=1,I2M1,2
5       B=B*DBLE(I)
      C=FACT(K)*(10.D0**KFAC(K))
6     D=DSQRT(A/B*C)
      EMRROD=D
      RETURN
      END
