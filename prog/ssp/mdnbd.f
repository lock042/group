C
C     ----- SOUS-PROGRAMME VALEURS DE M POUR G
C
      SUBROUTINE MDNBD(AJ,IC,IS,MPAS,AMMIN,AMMAX,MNB)
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      I2J=NINT(2*AJ)
      IF(IC.EQ.1) THEN
        MPAS=4
        IX=0
      ENDIF
      IF(IC.EQ.2) THEN
        MPAS=4
        IX=4
      ENDIF
      IF(IC.EQ.3) THEN
        MPAS=4
        IF(IS.EQ.1) IX=0
        IF(IS.EQ.2) IX=4
      ENDIF
      IF(IC.EQ.4) THEN
        MPAS=2
        IF(IS.EQ.1) IX=2
        IF(IS.EQ.2) IX=2
        IF(IS.EQ.3) THEN
          MPAS=4
          IX=0
        ENDIF
      ENDIF
      IF(IC.EQ.5) THEN
        MPAS=2
        IF(IS.EQ.1) IX=2
        IF(IS.EQ.2) IX=2
        IF(IS.EQ.3) THEN
          MPAS=4
          IX=4
        ENDIF
      ENDIF
      IF(IC.EQ.6) THEN
        MPAS=4
        IF(IS.EQ.1) IX=1
        IF(IS.EQ.2) IX=-1
      ENDIF
      IF(IC.EQ.7) THEN
        MPAS=4
        IF(IS.EQ.1) IX=5
        IF(IS.EQ.2) IX=3
      ENDIF
      IF(IC.EQ.8) THEN
        MPAS=4
        IF(IS.EQ.1) IX=3
        IF(IS.EQ.2) IX=1
        IF(IS.EQ.3) IX=-1
        IF(IS.EQ.4) IX=5
      ENDIF
      AMMAX=.5D0*(INT(DBLE(I2J-IX)/8.D0)*8.D0+IX)
      AMMIN=.5D0*(INT(DBLE(-I2J-IX)/8.D0)*8.D0+IX)
      IF(AMMAX.GT.AJ) AMMAX=AMMAX-MPAS
      IF(AMMIN.LT.-AJ) AMMIN=AMMIN+MPAS
      IF(NINT(AJ-AMMAX).GE.MPAS) AMMAX=AMMAX+MPAS
      IF(NINT(AMMIN+AJ).GE.MPAS) AMMIN=AMMIN-MPAS
      IF(IC.LE.5) AMMIN=-AMMAX
      MNB=INT(AMMAX-AMMIN)/MPAS+1
      RETURN
      END
